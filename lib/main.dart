import 'package:ecommerce/helper/ga_color.dart';
import 'package:ecommerce/pages/bottom_nav.dart';
import 'package:ecommerce/pages/create_account.dart';
import 'package:ecommerce/pages/home_page.dart';
import 'package:ecommerce/pages/login_page.dart';
import 'package:ecommerce/pages/onboarding_screen/onborading_page.dart';
import 'package:flutter/material.dart';

void main() async {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MaterialColor primarySwatch = MaterialColor(0xFF7BED8D, {
      50: ClsGAColor.getColor(color: GAColor.mainShade50),
      100: ClsGAColor.getColor(color: GAColor.mainShade100),
      200: ClsGAColor.getColor(color: GAColor.mainShade300),
      300: ClsGAColor.getColor(color: GAColor.mainShade300),
      400: ClsGAColor.getColor(color: GAColor.mainShade400),
      500: ClsGAColor.getColor(color: GAColor.mainShade500),
      600: ClsGAColor.getColor(color: GAColor.mainShade600),
      700: ClsGAColor.getColor(color: GAColor.mainShade700)
    });
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: const MainPage(),
      theme: ThemeData(
          primaryColor: ClsGAColor.getColor(color: GAColor.lightGrey),
          primarySwatch: primarySwatch,
          appBarTheme:
              AppBarTheme(color: ClsGAColor.getColor(color: GAColor.main))),
    );
  }
}
