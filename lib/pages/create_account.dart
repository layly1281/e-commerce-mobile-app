import 'package:ecommerce/helper/ga_appbar.dart';
import 'package:ecommerce/helper/ga_button.dart';
import 'package:ecommerce/helper/ga_color.dart';
import 'package:ecommerce/helper/ga_gesturedetector.dart';
import 'package:ecommerce/helper/ga_text.dart';
import 'package:ecommerce/helper/ga_textformfield.dart';
import 'package:ecommerce/helper/ga_variable.dart';
import 'package:ecommerce/main.dart';
import 'package:ecommerce/pages/login_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_svg/svg.dart';

class CreateAccountScreen extends StatefulWidget {
  const CreateAccountScreen({Key? key}) : super(key: key);

  @override
  State<CreateAccountScreen> createState() => _CreateAccountScreenState();
}

class _CreateAccountScreenState extends State<CreateAccountScreen> {
  // final DateTime _maxDate = DateTime();
  // final DateTime _minDate = DateTime.now();
  DateTime selectedDate = DateTime.now();
  @override
  Widget build(BuildContext context) {
    mainWidth = MediaQuery.of(context).size.width;
    mainHieght = MediaQuery.of(context).size.height;
    return Scaffold(
      body: ListView(
        children: [
          GAAppBar(
            rightIcon: Icons.close,
            onTap: () {},
            appBarHieght: mainWidth * 1.5,
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Column(
                children: [
                  const GAText(
                    text: 'CREATE ACCOUNT',
                    gaColor: GAColor.main,
                  ),
                  SizedBox(
                    height: mainWidth * wPaddingAll * 2,
                  ),
                  GATextFormField(
                    labelDesc: 'Full Name',
                    validationRules: GATextFormFieldValidationRules.text,
                    iconImage: svgPath + 'account.svg',
                  ),
                  SizedBox(
                    height: mainHieght * 0.02,
                  ),
                  GATextFormField(
                    labelDesc: 'Email',
                    validationRules: GATextFormFieldValidationRules.email,
                    iconImage: svgPath + 'mail.svg',
                  ),
                  SizedBox(
                    height: mainHieght * 0.02,
                  ),
                  GATextFormField(
                    labelDesc: 'Password',
                    validationRules: GATextFormFieldValidationRules.password,
                    isObSecure: false,
                    iconImage: svgPath + 'padlock.svg',
                  ),
                  SizedBox(
                    height: mainHieght * 0.02,
                  ),
                  GAText(
                    align: TextAlign.center,
                    text: 'Forget Password?',
                    color: Colors.grey,
                    // gaColor: GAColor.main,
                  ),
                  SizedBox(
                    height: mainWidth * wPaddingAll * 2,
                  ),
                  GAButton(
                      buttonType: GAButtonType.iconWithText,
                      title: "Sign In ",
                      onSubmit: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LoginScreen()));
                      })
                ],
              ),
            ),
          ),
          SizedBox(
            height: mainWidth * wPaddingAll * 3,
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LoginScreen()));
            },
            child: const GAText(
              align: TextAlign.center,
              text: 'sign in',
              isAllCapitalText: true,
              fontWeight: FontWeight.bold,
              gaColor: GAColor.grey,
            ),
          ),
        ],
      ),
    );
  }
}
