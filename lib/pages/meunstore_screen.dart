import 'dart:async';
import 'dart:convert';

import 'package:ecommerce/helper/ga_appbar.dart';
import 'package:ecommerce/helper/ga_color.dart';
import 'package:ecommerce/helper/ga_text.dart';
import 'package:ecommerce/helper/ga_variable.dart';
import 'package:ecommerce/models/product.dart';
import 'package:ecommerce/pages/login_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/src/material/bottom_navigation_bar.dart';
import '../helper/ga_button.dart';

class MeunStoreScreen extends StatefulWidget {
  const MeunStoreScreen({Key? key}) : super(key: key);

  @override
  State<MeunStoreScreen> createState() => _MeunStoreScreenState();
}

class _MeunStoreScreenState extends State<MeunStoreScreen> {
  DateTime selectedDate = DateTime.now();
  int currentPage = 0;
  late Timer _timer;
  final PageController _pageController = PageController(initialPage: 0);
  List<Product> _productItem = [];

  _setTimer() {
    _timer = Timer.periodic(const Duration(seconds: 10), (timer) {
      if (currentPage == _productItem.length - 1) {
        _pageController.animateTo(0,
            duration: const Duration(milliseconds: 300), curve: Curves.linear);
      } else {
        _pageController.animateToPage(currentPage + 1,
            duration: const Duration(milliseconds: 300), curve: Curves.linear);
      }
    });
  }

  void getItem() async {
    final respone = await rootBundle.loadString('assets/json/product.json');
    final data = json.decode(respone);
    List<Product> tmpList = [];
    for (var item in data) {
      Product product = Product.map(item);
      tmpList.add(product);
    }
    ;
  }

  @override
  Widget build(BuildContext context) {
    mainWidth = MediaQuery.of(context).size.width;
    mainHieght = MediaQuery.of(context).size.height;
    bool isSwitched = false;
    return Scaffold(
      // body: ListView(children: [
      //   Column(
      //     children: [
      //       GAAppBar(
      //         rightIcon: Icons.search,
      //         appBarHieght: mainWidth * 1.2,
      //         onTap: () {},
      //         title: 'Store',
      //         textColor: GAColor.grey,
      //         child: Column(
      //           children: [
      //             Image.asset('images/broccoli.png'),
      //             GAText(
      //               text: 'Vegetable',
      //               fontSize: GAFontSize.header1_30,
      //               gaColor: GAColor.mediumGrey,
      //             ),
      //             Row(
      //               children: [],
      //             ),
      //           ],
      //         ),
      //       ),
      //     ],
      //   ),
      //   SizedBox(
      //     height: mainWidth * wPaddingAll * 3,
      //   ),

      // ]),

      body: _bodyMeun(),
    );
  }
}

_bodyMeun() {
  return GAAppBar(
    appBarHieght: mainHieght * wPaddingAll * 1.3,
    title: 'dsfsdf',
    onTap: () {},
  );
}
