import 'package:ecommerce/helper/ga_color.dart';
import 'package:ecommerce/helper/ga_variable.dart';
import 'package:ecommerce/pages/cart/cart_page.dart';
import 'package:ecommerce/pages/home_page.dart';
import 'package:ecommerce/pages/meunstore_screen.dart';
import 'package:ecommerce/pages/recipe/recipe_post.dart';
import 'package:ecommerce/pages/setting/setting_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_svg/svg.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int currentPage = 0;
  List<Widget> tab = [
    MeunStoreScreen(),
    RecipePost(),
    CartPage(),
    SettingPage()
  ];
  final Color _colorGrey = ClsGAColor.getColor(color: GAColor.mediumGrey);
  final Color _colorMain = ClsGAColor.getColor(color: GAColor.main);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CupertinoTabScaffold(
          tabBar: _buildBottomNav(),
          tabBuilder: (context, index) {
            return tab[index];
          }),
      floatingActionButton: FloatingActionButton(
        backgroundColor: ClsGAColor.getColor(color: GAColor.mediumGrey),
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
        onPressed: (() {}),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  _buildBottomNav() {
    return CupertinoTabBar(
        iconSize: 40,
        currentIndex: currentPage,
        onTap: (index) {
          setState(() {
            currentPage = index;
          });
        },
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              'assets/svg/icon-store.svg',
              color: currentPage == 0 ? _colorMain : _colorGrey,
            ),
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              'assets/svg/icon-recipes.svg',
              color: currentPage == 1 ? _colorMain : _colorGrey,
            ),
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              'assets/svg/icon-cart.svg',
              color: currentPage == 2 ? _colorMain : _colorGrey,
            ),
          ),
          BottomNavigationBarItem(
            icon: SvgPicture.asset(
              'assets/svg/icon-settings.svg',
              color: currentPage == 3 ? _colorMain : _colorGrey,
            ),
          ),
        ]);
  }
}
