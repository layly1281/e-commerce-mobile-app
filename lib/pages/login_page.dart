import 'package:ecommerce/helper/ga_appbar.dart';
import 'package:ecommerce/helper/ga_button.dart';
import 'package:ecommerce/helper/ga_color.dart';
import 'package:ecommerce/helper/ga_gesturedetector.dart';
import 'package:ecommerce/helper/ga_text.dart';
import 'package:ecommerce/helper/ga_textformfield.dart';
import 'package:ecommerce/helper/ga_variable.dart';
import 'package:ecommerce/main.dart';
import 'package:ecommerce/pages/create_account.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  // final DateTime _maxDate = DateTime();
  // final DateTime _minDate = DateTime.now();
  DateTime selectedDate = DateTime.now();
  @override
  Widget build(BuildContext context) {
    mainWidth = MediaQuery.of(context).size.width;
    mainHieght = MediaQuery.of(context).size.height;
    return Scaffold(
      body: ListView(
        children: [
          GAAppBar(
            rightIcon: Icons.close,
            onTap: () {},
            appBarHieght: mainWidth * 1.5,
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Column(
                children: [
                  const GAText(
                    text: 'Sign In',
                    gaColor: GAColor.main,
                    isAllCapitalText: true,
                    fontWeight: FontWeight.bold,
                  ),
                  SizedBox(
                    height: mainWidth * wPaddingAll * 6,
                  ),
                  GATextFormField(
                    labelDesc: 'Email',
                    validationRules: GATextFormFieldValidationRules.email,
                    isObSecure: false,
                    iconImage: svgPath + 'mail.svg',
                    onChange: (value) {},
                  ),
                  SizedBox(
                    height: mainHieght * 0.03,
                  ),
                  GATextFormField(
                    labelDesc: 'Password',
                    validationRules: GATextFormFieldValidationRules.password,
                    isObSecure: false,
                    iconImage: svgPath + 'padlock.svg',
                    onShowPassword: () {},
                  ),
                  SizedBox(
                    height: mainHieght * 0.02,
                  ),
                  const GAText(
                    align: TextAlign.center,
                    text: 'Forget Password?',
                    color: Colors.grey,
                    // gaColor: GAColor.main,
                  ),
                  SizedBox(
                    height: mainWidth * wPaddingAll * 3.5,
                  ),
                  GAButton(
                      buttonType: GAButtonType.iconWithText,
                      title: "Sign In ",
                      onSubmit: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const LoginScreen()));
                      })
                ],
              ),
            ),
          ),
          SizedBox(
            height: mainWidth * wPaddingAll * 3,
          ),
          GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const CreateAccountScreen()));
            },
            child: const GAText(
              align: TextAlign.center,
              text: 'Create account',
              isAllCapitalText: true,
              fontWeight: FontWeight.bold,
              gaColor: GAColor.grey,
            ),
          ),
        ],
      ),
    );
  }
}
