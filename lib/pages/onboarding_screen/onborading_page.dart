import 'dart:convert';

import 'package:ecommerce/helper/ga_button.dart';
import 'package:ecommerce/helper/ga_color.dart';
import 'package:ecommerce/helper/ga_gesturedetector.dart';
import 'package:ecommerce/helper/ga_switch_button.dart';
import 'package:ecommerce/helper/ga_text.dart';
import 'package:ecommerce/helper/ga_variable.dart';
import 'package:ecommerce/models/intro.dart';
import 'package:ecommerce/models/switch.dart';
import 'package:ecommerce/pages/home_page.dart';
import 'package:ecommerce/pages/login_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class OnboradingScreen extends StatefulWidget {
  const OnboradingScreen({Key? key}) : super(key: key);

  @override
  State<OnboradingScreen> createState() => _OnboradingScreenState();
}

class _OnboradingScreenState extends State<OnboradingScreen> {
  List<Intro> introList = [];
  List<SwitchClass> _list = [];
  int currentPage = 0;
  late PageController _pageController;
  bool isSwitched = false;

  @override
  void initState() {
    fetchIntro();
    _pageController = PageController(initialPage: 0);
    // _pageController.addListener(() {});
    _fetchSwitch();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    mainWidth = MediaQuery.of(context).size.width;
    mainHieght = MediaQuery.of(context).size.height;

    return Scaffold(
        // backgroundColor: Colors.white,
        body: Stack(
      children: [
        Positioned(
          top: 0,
          left: 0,
          child: Container(
            height: mainHieght * 0.85,
            width: mainWidth,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(mainWidth * wPaddingAll * 1.5),
                    bottomRight:
                        Radius.circular(mainWidth * wPaddingAll * 1.5)),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                      blurRadius: 10,
                      spreadRadius: -5,
                      offset: const Offset(-8, 8),
                      color: Colors.grey.withOpacity(0.3))
                ]),
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.center,
                  child: PageView.builder(
                      itemCount: introList.length,
                      onPageChanged: (value) {
                        setState(() {
                          currentPage = value;
                          print(value);
                        });
                      },
                      controller: _pageController,
                      itemBuilder: (context, index) {
                        return SizedBox(
                          width: mainWidth,
                          height: mainHieght,
                          child: Stack(
                            children: [
                              currentPage == introList.length - 1
                                  ? Positioned(
                                      top: mainWidth * 0.28,
                                      left: mainWidth * 0.33,
                                      child: const GAText(
                                        text: 'Recipe Preferences',
                                        maxLines: 2,
                                        fontSize: GAFontSize.header_3_20,
                                        align: TextAlign.center,
                                        gaColor: GAColor.mediumGrey,
                                        color: Colors.black,
                                      ),
                                    )
                                  : Container(),
                              currentPage == introList.length - 1
                                  ? Positioned(
                                      top: mainWidth * 0.47,
                                      left: mainWidth * 0.092,
                                      child: Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal:
                                                mainWidth * wPaddingAll),
                                        width: mainWidth * 0.80,
                                        height: mainHieght * 0.7,
                                        child: Column(
                                            children: List.generate(
                                          _list.length,
                                          (index) => Container(
                                            width: mainWidth * 0.80,
                                            // height:
                                            //     mainWidth * wPaddingAll * 4.5,
                                            padding: EdgeInsets.only(
                                                bottom:
                                                    mainWidth * wPaddingAll),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: [
                                                GAText(
                                                  text: _list[index].title,
                                                  gaColor: GAColor.grey,
                                                ),
                                                Container(
                                                  width: mainWidth *
                                                      wPaddingAll *
                                                      3.58,
                                                  height: mainWidth *
                                                      wPaddingAll *
                                                      2.3,
                                                  child: GASwitch(
                                                      value:
                                                          _list[index].isSwitch,
                                                      onValueChanged: (value) {
                                                        setState(() {
                                                          _list[index]
                                                              .isSwitch = value;
                                                        });
                                                      }),
                                                )
                                              ],
                                            ),
                                          ),
                                        )),
                                      ),
                                    )
                                  : Align(
                                      alignment: Alignment.center,
                                      child:
                                          Image.asset(introList[index].image)),
                              Align(
                                alignment: Alignment.bottomCenter,
                                child: Padding(
                                  padding:
                                      EdgeInsets.only(bottom: mainWidth * 0.17),
                                  child: SizedBox(
                                    width: mainWidth * 0.6,
                                    height: mainWidth * 0.3,
                                    child: GAText(
                                      text: introList[index].title,
                                      maxLines: 2,
                                      fontSize: GAFontSize.header_2_24,
                                      align: TextAlign.center,
                                      gaColor: GAColor.mediumGrey,
                                      color: Colors.black,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        );
                      }),
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Padding(
                    padding: EdgeInsets.only(bottom: mainWidth * 0.1),
                    child: SizedBox(
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: List.generate(introList.length,
                              (index) => _buildIndicator(index))),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Positioned(
          bottom: currentPage == introList.length - 1
              ? mainWidth * wPaddingAll * 2
              : mainWidth * wPaddingAll * 4,
          left: currentPage == introList.length - 1
              ? mainWidth * wPaddingAll * 6.5
              : mainWidth * 0.45,
          child:
              // currentPage == 0
              //     ? Container()
              //     : Positioned(
              //         left: mainWidth * wPaddingAll * 1.5,
              //         child: GestureDetector(
              //             onTap: () {
              //               if (currentPage != 0) {
              //                 _pageController.previousPage(
              //                     duration: const Duration(milliseconds: 500),
              //                     curve: Curves.easeInOut);
              //               }
              //             },
              //             child: currentPage == introList.length - 1
              //                 ? null
              //                 : Icon(Icons.arrow_back_ios,
              //                     color: Colors.black,
              //                     size: mainWidth * wPaddingAll * 1.3)),
              //       ),
              currentPage == introList.length - 1
                  ? GAButton(
                      buttonType: GAButtonType.iconWithText,
                      title: "GET STARTED",
                      fontWeight: FontWeight.bold,
                      onSubmit: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LoginScreen()));
                      })
                  : GAGestureDetector(
                      onTap: () {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const LoginScreen()));
                      },
                      child: const GAText(
                        text: 'Skip',
                        isAllCapitalText: true,
                        fontWeight: FontWeight.bold,
                        gaColor: GAColor.grey,
                      ),
                    ),

          // Positioned(
          //   right: mainWidth * wPaddingAll * 1.5,
          //   child: GestureDetector(
          //       onTap: () {
          //         // setState(() {
          //         //   isButtonPress = true;
          //         // });
          //         if (currentPage == introList.length - 1) {
          //           Navigator.push(
          //               context,
          //               MaterialPageRoute(
          //                   builder: (context) => const HomePage()));
          //         } else {
          //           _pageController.nextPage(
          //               duration: const Duration(milliseconds: 500),
          //               curve: Curves.easeInOut);
          //         }
          //       },
          //       child: Icon(
          //         currentPage == introList.length - 1
          //             ? null
          //             : Icons.arrow_forward_ios,
          //         color: Colors.black,
          //         size: currentPage == introList.length - 1
          //             ? mainWidth * wPaddingAll * 1.7
          //             : mainWidth * wPaddingAll * 1.3,
          //       )),
          // )
        ),
      ],
    ));
  }

  fetchIntro() async {
    final response = await rootBundle.loadString('assets/json/intro.json');
    final responseData = json.decode(response);
    List<Intro> list = [];
    for (var item in responseData) {
      Intro intro = Intro.map(item);
      list.add(intro);
    }
    setState(() {
      introList = list;
    });
  }

  _buildIndicator(int index) {
    return Container(
      width: mainWidth * 0.025,
      height: mainWidth * 0.025,
      padding: EdgeInsets.symmetric(horizontal: mainWidth * wPaddingAll),
      margin: EdgeInsets.symmetric(horizontal: mainWidth * wPaddingAll * 0.3),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: currentPage == index
            ? ClsGAColor.getColor(color: GAColor.mediumGrey)
            : ClsGAColor.getColor(color: GAColor.lightGrey),
      ),
    );
  }

  _fetchSwitch() async {
    final response = await rootBundle.loadString('assets/json/switch.json');
    List<SwitchClass> swCls = [];
    final data = json.decode(response);
    for (var item in data) {
      SwitchClass switchClass = SwitchClass.map(item);
      swCls.add(switchClass);
    }
    setState(() {
      _list = swCls;
    });
  }
}
