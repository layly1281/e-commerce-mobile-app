import 'package:ecommerce/helper/ga_color.dart';
import 'package:ecommerce/helper/ga_variable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GASwitch extends StatelessWidget {
  final bool value;
  final Function(bool) onValueChanged;

  const GASwitch({Key? key, required this.value, required this.onValueChanged})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: mainWidth * wPaddingAll * 3.1,
      height: mainWidth * wPaddingAll * 2.1,
      // padding: EdgeInsets.symmetric(horizontal: mainWidth * wPaddingAll),
      decoration: BoxDecoration(
          // color: Colors.black,
          border: Border.all(
              color: ClsGAColor.getColor(color: GAColor.main), width: 1),
          borderRadius: BorderRadius.circular(mainWidth * wPaddingAll)),
      child: Stack(
        children: [
          Align(
            alignment: Alignment.centerRight,
            child: CupertinoSwitch(
              activeColor: ClsGAColor.getColor(color: GAColor.white),
              thumbColor: ClsGAColor.getColor(
                  color: value == false ? GAColor.mediumGrey : GAColor.main),
              trackColor: Colors.white,
              value: value,
              onChanged: (valueChange) {
                onValueChanged(valueChange);
              },
            ),
          ),
          value == false
              ? Positioned(
                  top: mainWidth * wPaddingAll * 0.57,
                  left: mainWidth * wPaddingAll * 0.60,
                  child: Icon(
                    Icons.clear,
                    color: Colors.white,
                    size: mainWidth * wPaddingAll,
                  ))
              : Container(),
          value == true
              ? Positioned(
                  top: mainWidth * wPaddingAll * 0.57,
                  right: mainWidth * wPaddingAll * 0.60,
                  child: Icon(
                    Icons.check,
                    color: Colors.white,
                    size: mainWidth * wPaddingAll,
                  ))
              : Container()
        ],
      ),
    );
  }
}
