import 'package:ecommerce/helper/ga_color.dart';
import 'package:ecommerce/helper/ga_text.dart';
import 'package:ecommerce/helper/ga_variable.dart';
import 'package:flutter/material.dart';

enum GAButtonType { text, icon, iconWithText }

class GAButton extends StatelessWidget {
  final String title;
  final VoidCallback onSubmit;
  final GAColor backgroundColor;
  final GAColor textColor;
  final GAColor iconColor;
  final IconData icon;
  final GAButtonType buttonType;
  final bool isFeedToWidth;
  final double? height;
  final FontWeight fontWeight;

  const GAButton({
    Key? key,
    this.title = '',
    required this.onSubmit,
    this.backgroundColor = GAColor.main,
    this.textColor = GAColor.white,
    this.iconColor = GAColor.white,
    this.buttonType = GAButtonType.text,
    this.icon = Icons.arrow_forward_rounded,
    this.isFeedToWidth = false,
    this.fontWeight = FontWeight.normal,
    this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    debugPrint('$textColor, $title,');

    return Padding(
      padding: EdgeInsets.all(mainWidth * wPaddingAll * 1),
      child: ElevatedButton(
        onPressed: () {
          onSubmit();
        },
        style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all<Color>(
                ClsGAColor.getColor(color: backgroundColor)),
            padding: MaterialStateProperty.all(
                EdgeInsets.all(mainWidth * wPaddingAll * 1.2)),
            shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
              borderRadius:
                  BorderRadius.circular(mainWidth * wPaddingAll * 2.5),
            ))),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          // crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            buttonType == GAButtonType.icon ||
                    buttonType == GAButtonType.iconWithText
                ? Icon(
                    icon,
                    color: ClsGAColor.getColor(color: iconColor),
                    size: mainWidth * wPaddingAll,
                  )
                : Container(),
            SizedBox(
              width: mainWidth * wPaddingAll,
            ),
            buttonType == GAButtonType.text ||
                    buttonType == GAButtonType.iconWithText
                ? Padding(
                    padding:
                        EdgeInsets.only(top: mainWidth * wPaddingAll * 0.3),
                    child: GAText(
                      text: title,
                      gaColor: textColor,
                      isAllCapitalText: true,
                      fontFamily: GAFontFamily.acumin,
                      fontSize: GAFontSize.bodySmall14,
                      fontWeight: fontWeight,
                      // color: Colors.white,
                      align: TextAlign.center,
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }
}
