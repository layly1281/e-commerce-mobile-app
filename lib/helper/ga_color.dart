import 'package:flutter/material.dart';

enum GAColor {
  darkGrey,
  darkerGrey,
  lightGrey,
  lighterGrey,
  mediumGrey,
  grey,
  main,
  white,
  tranparent,

  mainShade50,
  mainShade100,
  mainShade200,
  mainShade300,
  mainShade400,
  mainShade500,
  mainShade600,
  mainShade700,

  black,
  none,

  unSuccessOrder,
}

class ClsGAColor {
  ClsGAColor();
  static GAColor getColorsByName(
      {String colorName = '', String colorPrefix = 'status'}) {
    for (int i = 0; i < GAColor.values.length; i++) {
      var arr = GAColor.values[i].toString().split(colorPrefix);
      var currentColorName = arr.length <= 1
          ? 'Not Found'
          : (arr[1][0].toString() + arr[1].substring(1));

      if (colorName.toUpperCase() == (currentColorName).toUpperCase()) {
        return GAColor.values[1];
      }
    }
    return GAColor.main;
  }

  static Color getColor(
      {GAColor color = GAColor.main, Color tmp = Colors.white}) {
    switch (color) {
      case GAColor.darkerGrey:
        return const Color(0xFF404E5A);
      case GAColor.darkGrey:
        return const Color(0xFF4E5D6A);
      case GAColor.grey:
        return const Color(0xFFA6BCD0);
      case GAColor.lightGrey:
        return const Color(0xFFDBE2ED);
      case GAColor.lighterGrey:
        return const Color(0xFFF0F4F8);
      case GAColor.main:
        return const Color(0xFF7BED8D);
      case GAColor.mainShade50:
        return const Color(0xFFD7FADD);
      case GAColor.mainShade100:
        return const Color(0xFFCAF8D1);
      case GAColor.mainShade200:
        return const Color(0xFFBDF6C6);
      case GAColor.mainShade300:
        return const Color(0xFFB0F4BB);
      case GAColor.mainShade400:
        return const Color(0xFFA3F2AF);
      case GAColor.mainShade500:
        return const Color(0xFF95F1A4);
      case GAColor.mainShade600:
        return const Color(0xFF88EF98);
      case GAColor.mainShade700:
        return const Color(0xFF7BED8D);
      case GAColor.mediumGrey:
        return const Color(0xFF748A9D);

      case GAColor.white:
        return Colors.white;

      case GAColor.black:
        return Colors.black;
      case GAColor.unSuccessOrder:
        return const Color(0xFFA10000);

      case GAColor.none:
        return const Color.fromRGBO(0, 0, 0, 0);
      default:
        return const Color(0xFFFFFFFF);
    }
  }
}
