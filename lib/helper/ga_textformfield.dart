import 'package:ecommerce/helper/ga_color.dart';
import 'package:ecommerce/helper/ga_text.dart';
import 'package:ecommerce/helper/ga_validation.dart';
import 'package:ecommerce/helper/ga_variable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:flutter_svg/flutter_svg.dart';

String pattern =
    r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z ]{2,}))$';

enum GATextFormFieldValidationRules {
  text,
  name,
  number,
  email,
  phone,
  password,
  confirmPassword,
  activation,
  custom,
  emailOrPhone,
  multiline,
  note,
  singlelineLongText,
  oldPassword,
}

class GATextFormField extends StatelessWidget {
  final GATextFormFieldValidationRules validationRules;
  final bool isShowBorder;
  final bool isChangePassword;
  final bool isRequired;
  final bool isAutofocus;
  final bool enabled;
  final AutovalidateMode autoValidateMode;
  final bool isAutoCorrect;
  final bool isTextChange;
  final bool isObSecure;
  final bool isSearch;
  final bool isFillBackground;
  final bool isShawdow;

  final int inputFormatter;
  final String labelDesc;
  final GAColor labelColor;
  final GAColor textColor;
  final String iconImage;
  final GAColor borderColor;
  final GAFontFamily fontFamily;
  final GAFontSize fontSize;
  final FontWeight fontWeight;
  final TextEditingController? controller;
  final FocusNode? focusNode;
  final String defaultValue;
  final TextInputType? inputType;
  final TextAlign textAlign;

  final ValueChanged<String?>? onSaved;
  final ValueChanged<Map<String, String>>? onValidating;
  final VoidCallback? onEditingComplete;
  final ValueChanged<String>? onFieldSubmitted;
  final VoidCallback? onShowPassword;
  final ValueChanged<bool>? onTextChange;
  final Function()? onTap;
  final ValueChanged<String>? onChange;
  final VoidCallback? onSearch;
  const GATextFormField(
      {Key? key,
      this.validationRules = GATextFormFieldValidationRules.text,
      this.isShowBorder = false,
      this.isChangePassword = false,
      this.isRequired = true,
      this.isAutofocus = false,
      this.enabled = true,
      this.autoValidateMode = AutovalidateMode.disabled,
      this.isAutoCorrect = true,
      this.isTextChange = false,
      this.isObSecure = false,
      this.isSearch = false,
      this.isFillBackground = true,
      this.isShawdow = false,
      this.inputFormatter = 50,
      this.labelDesc = '',
      this.labelColor = GAColor.black,
      this.textColor = GAColor.black,
      this.iconImage = '',
      this.borderColor = GAColor.black,
      this.fontFamily = GAFontFamily.acumin,
      this.controller,
      this.focusNode,
      this.defaultValue = '',
      this.inputType,
      this.onSaved,
      this.onValidating,
      this.onEditingComplete,
      this.onFieldSubmitted,
      this.onShowPassword,
      this.onTextChange,
      this.onTap,
      this.onChange,
      this.onSearch,
      this.fontSize = GAFontSize.bodyFont16,
      this.fontWeight = FontWeight.normal,
      this.textAlign = TextAlign.left})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextInputType _textInputType = TextInputType.text;
    TextInputAction _textInputAction = TextInputAction.unspecified;
    TextStyle _style = GAText.getTextStyle(
        fontFamily: GAFontFamily.acumin,
        fontSize: fontSize,
        fontWeight: fontWeight);
    // TextAlign _align = TextAlign.left;
    int _intMaxTextLength = 50;

    if (validationRules == GATextFormFieldValidationRules.number) {
      _textInputType = TextInputType.number;
    } else if (validationRules == GATextFormFieldValidationRules.text) {
      _textInputType = TextInputType.text;
    } else if (validationRules == GATextFormFieldValidationRules.email) {
      _textInputType == TextInputType.emailAddress;
    } else if (validationRules == GATextFormFieldValidationRules.name) {
      _textInputType = TextInputType.text;
      _intMaxTextLength = 25;
    } else if (validationRules == GATextFormFieldValidationRules.oldPassword) {
      _textInputType = TextInputType.text;
    } else if (validationRules == GATextFormFieldValidationRules.password) {
      _textInputType = TextInputType.text;
    } else if (validationRules == GATextFormFieldValidationRules.multiline) {
      _textInputType = TextInputType.text;
      _intMaxTextLength = 200;
    } else if (validationRules ==
        GATextFormFieldValidationRules.confirmPassword) {
      _textInputType = TextInputType.text;
    } else if (validationRules == GATextFormFieldValidationRules.note) {
      _textInputType = TextInputType.text;
      _intMaxTextLength = 500;
    } else {
      _textInputType = TextInputType.text;
      _intMaxTextLength;
    }

    if (controller != null) {
      controller!.addListener(() {
        // print('controller.text: ${controller.text}');
        if (controller!.text.isNotEmpty) {
          onTextChange!(true);
        } else {
          onTextChange!(false);
        }
      });
    }

    return Container(
      padding: EdgeInsets.all(mainWidth * wPaddingAll * 0.90),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(mainWidth * wPaddingAll),
          color: ClsGAColor.getColor(
              color: isShawdow == true ? GAColor.white : GAColor.lighterGrey),
          border: isFillBackground
              ? null
              : Border.all(
                  width: 1,
                  color: ClsGAColor.getColor(color: GAColor.mediumGrey))),
      alignment: Alignment.center,
      child: TextFormField(
        focusNode: focusNode,
        autocorrect: isAutoCorrect,
        autofocus: isAutofocus,
        autovalidateMode: autoValidateMode,
        controller: controller,
        decoration: _generateInputDecoration(labelDesc, labelColor, context),
        style: _style,
        enabled: enabled,
        inputFormatters: [LengthLimitingTextInputFormatter(_intMaxTextLength)],
        key: key,
        enableInteractiveSelection: true,
        textAlign: textAlign,
        textCapitalization: TextCapitalization.none,
        keyboardType: _textInputType,
        textInputAction: _textInputAction,
        initialValue: controller == null ? defaultValue : null,
        validator: (value) {
          return _onTextFormFieldValidation(value!, context);
        },
        obscureText: isObSecure,
        onSaved: onSaved,
        onChanged: onChange,
        onTap: onTap,
        onFieldSubmitted: (value) {
          if (onFieldSubmitted != null) onFieldSubmitted!(value);
        },
        onEditingComplete: () {
          FocusScope.of(context).requestFocus(FocusNode());
          if (onEditingComplete != null) onEditingComplete!();
        },
      ),
    );
  }

  InputDecoration _generateInputDecoration(
      String labelText, GAColor labelHint, BuildContext context) {
    final errorOutlineBorder = !isShowBorder
        ? null
        : OutlineInputBorder(
            borderSide: BorderSide(
                style: BorderStyle.solid,
                width: 1,
                color: ClsGAColor.getColor(color: GAColor.unSuccessOrder)),
            borderRadius: BorderRadius.circular(0));

    final normalOutlineBorder = !isShowBorder
        ? null
        : OutlineInputBorder(
            borderSide: BorderSide(
                style: BorderStyle.solid,
                width: 1,
                color: ClsGAColor.getColor(color: borderColor)),
            borderRadius: BorderRadius.circular(5));

    return InputDecoration(
      suffixIcon: enabled || isTextChange
          ? isChangePassword
              // show password
              ? IconButton(
                  iconSize: GASize.getTextSize(GAFontSize.bodyFont16),
                  onPressed: () {
                    if (onShowPassword != null) onShowPassword!();
                    // print(isShowPassword);
                  },
                  color: ClsGAColor.getColor(color: GAColor.grey),
                  icon: Icon(
                    isObSecure ? Icons.visibility : Icons.visibility_off,
                    color: ClsGAColor.getColor(color: GAColor.main),
                    size: mainWidth * wPaddingAll * 1.3,
                  ),
                )

              // clear text
              : isSearch
                  ? Container(
                      margin: EdgeInsets.only(top: mainWidth * 0.02),
                      child: IconButton(
                        onPressed: () => {onSearch!()},
                        icon: const ImageIcon(AssetImage('images/search.png')),
                        color: Colors.black,
                        iconSize: mainWidth * wPaddingAll * 1.2,
                      ),
                    )
                  : IconButton(
                      iconSize: !isTextChange
                          ? 0
                          : mainWidth *
                              GASize.getTextSize(GAFontSize.bodyFont16),
                      onPressed: () {
                        controller?.clear();
                      },
                      color: ClsGAColor.getColor(color: GAColor.grey),
                      icon: const Icon(Icons.clear),
                    )
          : Container(),
      fillColor: ClsGAColor.getColor(
          color: (!enabled && textColor == GAColor.white)
              ? GAColor.lightGrey
              : textColor),
      hintText: labelText,
      prefixIcon: Padding(
        padding: EdgeInsets.all(mainWidth * wPaddingAll * 0.6),
        child: SvgPicture.asset(
          iconImage,
          // width: mainWidth * wPaddingAll,
          // height: mainWidth * wPaddingAll,
          // fit: BoxFit.contain,
          color: ClsGAColor.getColor(color: GAColor.mediumGrey),
        ),
      ),
      hintStyle: GAText.getTextStyle(
          fontFamily: GAFontFamily.acumin,
          color: ClsGAColor.getColor(color: labelColor),
          fontWeight: fontWeight,
          fontSize: fontSize),
      enabledBorder: normalOutlineBorder,
      border: InputBorder.none,
      focusedBorder: normalOutlineBorder,
      focusedErrorBorder: errorOutlineBorder,
      errorBorder: errorOutlineBorder,
      alignLabelWithHint: true,
      contentPadding: EdgeInsets.only(
          top: mainWidth * textboxPadding * 1.4, left: mainWidth * wPaddingAll),
      filled: !isShowBorder ? false : true,
    );
  }

  String? _onTextFormFieldValidation(String value, BuildContext context) {
    String errMsg = '';
    switch (validationRules) {

      ////////////////////
      /// email validation
      case GATextFormFieldValidationRules.email:
        RegExp regExp = RegExp(pattern);
        if (isRequired) {
          if (value.trim().isEmpty) {
            // errMsg = RCLanguage.translate(context, 'errInputEmailRequired');
          }
          if (!regExp.hasMatch(value)) {
            // errMsg = RCLanguage.translate(context, 'errInputEmail');
          }
        }

        break;

      ////////////////////
      /// phone validation
      case GATextFormFieldValidationRules.phone:
        final RegExp phoneExp = RegExp(r'(^[0-9]*$)');

        if (isRequired) {
          if (value.trim().length < 9) {
            // errMsg = RCLanguage.translate(context, 'errInputPhoneGE9');
          } else if (!phoneExp.hasMatch(value)) {
            // errMsg = RCLanguage.translate(context, 'errInputPhone09');
          }
        }

        break;

      ////////////////////
      /// name validation
      case GATextFormFieldValidationRules.name:
        // String pattern = r'(^[a-zA-Z0-9\s\_]*$)';
        // RegExp regExp = new RegExp(pattern);

        if (value.length < 4) {
          // errMsg = RCLanguage.translate(context, 'errInputNameGE4');
        }
        // else if (!regExp.hasMatch(value))
        //   this._errMsg = RCLanguage.translate(context, 'errInputNameAZ');

        break;

      ////////////////////
      /// password validation
      case GATextFormFieldValidationRules.password:
        passwordMatch = value;
        String pattern = r'(^[a-zA-Z0-9\s]*$)';
        RegExp regExp = RegExp(pattern);
        if (value.length < 6) {
          // errMsg = RCLanguage.translate(context, 'errInputPasswordGE6');
        } else if (!regExp.hasMatch(value)) {
          // errMsg = RCLanguage.translate(context, 'errInputPasswordAZ');
        }

        break;

      //////
      /// confirm password validation
      case GATextFormFieldValidationRules.confirmPassword:
        if (value != passwordMatch) {
          // errMsg =
          //     RCLanguage.translate(context, 'errInputConfirmPasswordMatch');
        }

        break;

      ////////////////////
      /// activate validation
      case GATextFormFieldValidationRules.activation:
        String pattern = r'(^[0-9]*$)';
        RegExp regExp = RegExp(pattern);
        if (value.length < 6) {
          // errMsg = RCLanguage.translate(context, 'errInputActivation6');
        } else if (!regExp.hasMatch(value)) {
          // errMsg = RCLanguage.translate(context, 'errInputActivation09');
        }

        break;

      ////////////////////
      /// custom validation
      case GATextFormFieldValidationRules.custom:
        break;

      ////////////////////
      /// email or phone in one box validation
      case GATextFormFieldValidationRules.emailOrPhone:
        final RegExp phoneExp = RegExp(r'(^[0-9]*$)');

        if (isRequired) {
          /// check number or not
          if (GAValidation.isNumber(value)) {
            /// is number
            if (value.trim().length < 9) {
              // errMsg = RCLanguage.translate(context, 'errInputPhoneGE9');
            } else if (!phoneExp.hasMatch(value)) {
              // errMsg = RCLanguage.translate(context, 'errInputPhone09');
            }
          } else {
            /// not number is email
            RegExp regExp = RegExp(pattern);

            if (isRequired && value.trim().isEmpty) {
              // errMsg = RCLanguage.translate(context, 'errInputEmailRequired');
            }
            if (!regExp.hasMatch(value)) {
              // errMsg = RCLanguage.translate(context, 'errInputEmail');
            }
          }
        } else {
          if (GAValidation.isNumber(value)) {
            if (value.trim().isNotEmpty && !phoneExp.hasMatch(value)) {
              // errMsg = RCLanguage.translate(context, 'errInputPhone09');
            }
          }
        }

        break;

      ////////////////////
      /// multiline validation
      case GATextFormFieldValidationRules.multiline:
        break;

      ////////////////////
      /// Other case
      default:

        ///If reached, assume that your application is abnormally functioned!
        // errMsg = RCLanguage.translate(context, 'errInputUnexpect');
        break;
    }

    Map<String, String> responseMap = <String, String>{};
    responseMap['value'] = value;
    responseMap['error'] = errMsg;
    if (errMsg == '') {
      if (onValidating != null) onValidating!(responseMap);
      return null;
    }

    if (onValidating != null) onValidating!(responseMap);
    return '';
  }
}
