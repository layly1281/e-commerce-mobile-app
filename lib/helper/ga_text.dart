import 'package:ecommerce/helper/ga_color.dart';
import 'package:flutter/material.dart';

enum GAFontSize {
  bodyFont16,
  header_2_24,
  header_3_20,
  bottomFontBold14,
  linkBold14,
  header1_30,
  bodySmall14
}
enum GAFontFamily {
  acumin,
  montserrat,
  poppins,
  quicksand,
  sourceSansPro,
  buttonFont
}

class GAFont {
  GAFont();
  static String getTextFont(GAFontFamily fontFamily) {
    switch (fontFamily) {
      case GAFontFamily.acumin:
        return 'Acumin Pro SemiCond';

      case GAFontFamily.montserrat:
        return 'Monsterrat';

      case GAFontFamily.poppins:
        return 'Poppins';

      case GAFontFamily.quicksand:
        return 'Quicksand';

      case GAFontFamily.sourceSansPro:
        return 'Source Sans Pro';
      case GAFontFamily.buttonFont:
        return 'Sans Serif';
      default:
        return 'Poppins';
    }
  }
}

class GASize {
  GASize();
  static double getTextSize(GAFontSize size) {
    switch (size) {
      case GAFontSize.bodyFont16:
        return 16;
      case GAFontSize.bodySmall14:
        return 14;
      case GAFontSize.bottomFontBold14:
        return 14;
      case GAFontSize.header1_30:
        return 30;
      case GAFontSize.header_2_24:
        return 24;
      case GAFontSize.header_3_20:
        return 20;
      case GAFontSize.linkBold14:
        return 14;
      default:
        return 14;
    }
  }
}

class GAText extends StatelessWidget {
  final String text;
  final TextOverflow textOverflow;
  final TextAlign align;
  final GAColor? gaColor;
  final Color? color;
  final GAFontFamily fontFamily;
  final GAFontSize fontSize;
  final String semaStringabel;
  final int maxLines;
  final TextDecoration decoration;
  final bool isAllCapitalText;
  final FontWeight fontWeight;
  const GAText(
      {Key? key,
      required this.text,
      this.textOverflow = TextOverflow.fade,
      this.align = TextAlign.start,
      this.gaColor,
      this.color,
      this.fontFamily = GAFontFamily.acumin,
      this.fontSize = GAFontSize.bodyFont16,
      this.semaStringabel = '',
      this.maxLines = 1,
      this.isAllCapitalText = false,
      this.decoration = TextDecoration.none,
      this.fontWeight = FontWeight.normal})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      isAllCapitalText ? text.toUpperCase() : text,
      overflow: textOverflow,
      textAlign: align,
      maxLines: maxLines,
      softWrap: true,
      semanticsLabel: semaStringabel,
      style: getTextStyle(
        fontFamily: fontFamily,
        color: gaColor == null ? color : ClsGAColor.getColor(color: gaColor!),
        fontSize: fontSize,
        decoration: decoration,
        fontWeight: fontWeight,
      ),
    );
  }

  static getTextStyle({
    GAFontFamily fontFamily = GAFontFamily.acumin,
    Color? color,
    GAColor? gaColor,
    GAFontSize fontSize = GAFontSize.bodyFont16,
    TextDecoration decoration = TextDecoration.none,
    FontWeight fontWeight = FontWeight.normal,
  }) {
    double size = GASize.getTextSize(fontSize);
    return TextStyle(
      fontFamily: GAFont.getTextFont(fontFamily),
      fontWeight: fontWeight,
      fontSize: size,
      color: color,
      decoration: decoration,
    );
  }
}
