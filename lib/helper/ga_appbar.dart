import 'dart:io';

import 'package:ecommerce/helper/ga_color.dart';
import 'package:ecommerce/helper/ga_text.dart';
import 'package:ecommerce/helper/ga_variable.dart';
import 'package:flutter/material.dart';

class GAAppBar extends StatelessWidget {
  final String title;
  final IconData leftIcon;
  final IconData rightIcon;
  final VoidCallback? onCancel;
  final VoidCallback onTap;
  final bool isNavigate;
  final bool isHasShadow;
  final GAColor iconColor;
  final GAColor appBarColor;
  final double? appBarHieght;
  final Widget? child;
  final GAColor textColor;

  const GAAppBar(
      {Key? key,
      this.title = '',
      this.leftIcon = Icons.arrow_back_ios_new,
      this.rightIcon = Icons.delete,
      this.onCancel,
      required this.onTap,
      this.isNavigate = false,
      this.isHasShadow = true,
      this.appBarColor = GAColor.white,
      this.appBarHieght,
      this.child,

      ////////
      ///Color
      this.iconColor = GAColor.mediumGrey,
      this.textColor = GAColor.main})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double _appBarHeight = mainHieght * 0.2;
    return Container(
      width: mainWidth,
      height: appBarHieght == null ? _appBarHeight : appBarHieght!,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(mainWidth * wPaddingAll * 1.5),
              bottomRight: Radius.circular(mainWidth * wPaddingAll * 1.5)),
          color: ClsGAColor.getColor(color: appBarColor),
          boxShadow: [
            BoxShadow(
                blurRadius: 10,
                spreadRadius: -5,
                offset: const Offset(-8, 8),
                color: Colors.grey.withOpacity(0.3))
          ]),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                !isNavigate
                    ? const SizedBox()
                    : IconButton(
                        onPressed: () {
                          Navigator.pop(context);
                          onCancel!();
                        },
                        icon: Icon(
                          Platform.isIOS ? leftIcon : Icons.arrow_back_rounded,
                          color: ClsGAColor.getColor(color: iconColor),
                        )),
                Padding(
                  padding: EdgeInsets.only(left: mainWidth * wPaddingAll * 4),
                  child: GAText(
                    text: title,
                  ),
                ),
                isNavigate
                    ? Container()
                    : IconButton(
                        onPressed: () {
                          onTap();
                        },
                        icon: Icon(
                          rightIcon,
                          color: ClsGAColor.getColor(color: iconColor),
                        ))
              ],
            ),
          ),
          child!
        ],
      ),
    );
  }
}
