import 'package:flutter/material.dart';

class GAGestureDetector extends StatelessWidget {
  final VoidCallback onTap;
  final VoidCallback? onLongPress;
  final VoidCallback? onDoubleTap;
  final Widget child;
  const GAGestureDetector({
    Key? key,
    required this.onTap,
    this.onLongPress,
    this.onDoubleTap,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onDoubleTap: () {
        debugPrint('OnDoubleTap =====================>');
        onDoubleTap!();
      },
      onTap: () {
        debugPrint('OnTap =====================>');
        onTap();
      },
      onLongPress: () {
        debugPrint('OnLongPress =====================>');
        onLongPress!();
      },
      child: child,
    );
  }
}
