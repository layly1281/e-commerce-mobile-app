import 'package:ecommerce/helper/ga_color.dart';
import 'package:ecommerce/helper/ga_text.dart';
import 'package:ecommerce/helper/ga_variable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GAUI {
  static showGASnackbar({
    required BuildContext context,
    bool isSuccess = true,
    bool isFalse = true,
    IconData icon = Icons.info_outline,
    String description = '',
    int duration = 10,
    String actionLabel = '',
    VoidCallback? onTap,
  }) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [Icon(icon), GAText(text: description)],
        ),
        backgroundColor: ClsGAColor.getColor(
            color: isSuccess
                ? GAColor.main
                : isFalse
                    ? GAColor.unSuccessOrder
                    : GAColor.darkGrey),
        duration: Duration(seconds: duration),
        action: SnackBarAction(
          label: actionLabel,
          onPressed: onTap!,
        )));
  }

  static showGADialog(
      {required BuildContext context,
      String content = '',
      String fAction = '',
      String sAction = '',
      VoidCallback? fOnTap,
      VoidCallback? sOnTap}) {
    showCupertinoDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            content: GAText(text: content),
            actions: [
              CupertinoDialogAction(
                child: GAText(text: fAction),
                onPressed: fOnTap,
              ),
              CupertinoDialogAction(
                child: GAText(text: sAction),
                onPressed: sOnTap,
              )
            ],
          );
        });
  }

  static showGADatePicker(
      {required BuildContext context,
      Function(DateTime)? onDateChange,
      DateTime? maxDate,
      DateTime? minDate,
      int minYear = 1970,
      int maxYear = 2005,
      required DateTime initDate,
      CupertinoDatePickerMode mode = CupertinoDatePickerMode.date,
      VoidCallback? onDone}) {
    showModalBottomSheet(
        context: context,
        builder: (context) {
          return Container(
            height: mainHieght * 0.3,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(mainWidth * 0.05),
                    topRight: Radius.circular(mainWidth * 0.05))),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                CupertinoButton(
                    child: const GAText(
                      text: 'Done',
                      color: Colors.blueAccent,
                    ),
                    onPressed: onDone),
                Expanded(
                  child: CupertinoDatePicker(
                    onDateTimeChanged: onDateChange!,
                    maximumDate: maxDate,
                    minimumDate: minDate,
                    initialDateTime: initDate,
                    mode: mode,
                    minimumYear: minYear,
                    maximumYear: maxYear,
                    dateOrder: DatePickerDateOrder.dmy,
                  ),
                ),
              ],
            ),
          );
        });
  }
}
