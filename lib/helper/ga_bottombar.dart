import 'package:ecommerce/helper/ga_color.dart';
import 'package:ecommerce/helper/ga_text.dart';
import 'package:flutter/material.dart';

class GABottomAppBarItem {
  final String text;
  final IconData iconData;
  GABottomAppBarItem({this.iconData = Icons.home, this.text = ''});
}

class GAButtomAppBar extends StatefulWidget {
  final List<GABottomAppBarItem>? items;
  final String centerItemText;
  final double height;
  final double iconSize;
  final GAColor backgroundColor;
  final GAColor color;
  final GAColor selectedColor;
  final NotchedShape? notchedShape;
  final ValueChanged<int>? onTabSelected;
  const GAButtomAppBar(
      {Key? key,
      this.items,
      this.centerItemText = "",
      this.height = 0.3,
      this.iconSize = 0.3,
      this.backgroundColor = GAColor.grey,
      this.color = GAColor.main,
      this.selectedColor = GAColor.main,
      this.notchedShape,
      this.onTabSelected})
      : super(key: key);

  @override
  State<GAButtomAppBar> createState() => _GAButtomAppBarState();
}

class _GAButtomAppBarState extends State<GAButtomAppBar> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
