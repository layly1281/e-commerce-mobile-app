class Product {
  int _id = 0;
  String _name = '';
  double _price = 0.0;
  String _image = '';
  double _discount = 0.0;

  Product(
      {int id = 0,
      String name = '',
      double price = 0.0,
      String image = '',
      double discount = 0.0}) {
    _id = id;
    _name = name;
    _price = price;
    _image = image;
    _discount = discount;
  }
  int get id => _id;
  String get name => _name;
  double get price => _price;
  String get image => _image;
  double get discount => _discount;

  set id(int value) => _id = value;
  set name(String value) => _name = value;
  set price(double value) => _price = value;
  set image(String value) => _image = value;
  set discount(double value) => _discount = value;

  Product.map(dynamic obj) {
    _id = int.parse(obj['id']).toString() == ''
        ? 0
        : int.parse(obj['id'].toString());
    _name = obj['name'] == null ? '' : obj['name'].toString();
    _price = double.parse(obj['price']).toString() == ''
        ? 0.0
        : double.parse(obj['price'].toString());
    _image = obj['image'] == null ? '' : obj['image'].toString();
    _discount = double.parse(obj['discount']).toString() == ''
        ? 0.0
        : double.parse(obj['discount'].toString());
  }
}
