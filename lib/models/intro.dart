class Intro {
  int _id = 0;
  String _title = '';
  String _image = '';

  Intro({int id = 0, String title = '', String image = ''}) {
    _id = id;
    _title = title;
    _image = image;
  }
  ///////////
  ///GETTER
  int get id => _id;
  String get title => _title;
  String get image => _image;

  /////////
  ///SETTER
  set id(int value) => _id = value;
  set title(String value) => _title = value;
  set image(String value) => _image = value;

  Intro.map(dynamic obj) {
    _id = obj['id'] == null ? 0 : int.parse(obj['id'].toString());
    _title = obj['title'] == null ? '' : obj['title'].toString();
    _image = obj['image'] == null ? '' : obj['image'].toString();
  }
}
