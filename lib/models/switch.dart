class SwitchClass {
  late bool _isSwitch;
  late String _title;

  SwitchClass({bool isSwitch = false, String title = ''}) {
    _isSwitch = isSwitch;
    _title = title;
  }

  bool get isSwitch => _isSwitch;
  String get title => _title;

  set isSwitch(bool value) => _isSwitch = value;
  set title(String value) => _title = value;

  SwitchClass.map(dynamic obj) {
    _isSwitch = int.parse(obj['is_selected']) == 0 ? false : true;
    _title = obj['title'] == null ? '' : obj['title'].toString();
  }

}
